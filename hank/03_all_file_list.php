<?php

// Input path
$handle = readline('Input a local path: ');

$result = null;

getFileList($handle);
//getFileList('/Users/hank/php-practice');

function getFileList($path) {
    global $result;

    if (file_exists($path)) {
        $fileList = glob($path . '/*');

        foreach($fileList as $file){
            $result .= $file . PHP_EOL;

            if (is_dir($file)) {
                getFileList($file);
            }
         }
    }
}

print_r($result);
