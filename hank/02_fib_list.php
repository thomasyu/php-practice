<?php

// Input number
$num = readline('Enter a number: ');

// fib function
function fib($n){
    if($n == 0) 
        return 0;

    if($n == 1) 
        return 1;

    return fib($n-1) + fib($n-2); 
}

$result = null;
$i = 0;

// concat string
while (true) {
    $fibNum = fib($i);

    if($fibNum <= $num) {
        $result .= $fibNum . ' ';
        $i++;
    } else {
        break;
    }
}

print_r(($result === null ? 'No result matched' : 'Result: ' . $result) . PHP_EOL);
